const database = require('../utils/database');
exports.list = () => database.find('cavs');

exports.listAgenda = (cavId, date, category) => {
    const calendar = database.find('calendar');
    const agenda = calendar.date[date] && calendar.date[date].cav[cavId] && calendar.date[date].cav[cavId][category];
    const ret = {};

    for (const space in agenda) {
        if (Object.entries(agenda[space]).length === 0){
            ret[space] = 'livre';
        }
    }

    return ret;
};

exports.schedule = (category, cavId, carId, date, hour) => {
    const calendar = database.find('calendar');
    const car = database.find('cars', {id: carId});
    const agenda = calendar.date[date];
    if (car.length === 0) {
        return {status: 'error', msg: 'Carro não está cadastrado'}
    }
    if (agenda) {
       const agendaCav = calendar.date[date].cav[cavId] && calendar.date[date].cav[cavId][category];
       if (agendaCav) {
           if (agendaCav[hour] && Object.entries(agendaCav[hour]).length === 0) {
               const toSave = agendaCav[hour]['car'] = carId;
               database.update('calendar', toSave);
               return {status: 'success', msg: 'Marcado com sucesso!'}
           } else {
               return {status: 'error', msg: 'Horário escolhido não está disponível'}
           }
       } else {
           return {status: 'error', msg: 'Não existe data disponível para o cav escolhido'}
       }
    } else {
        return {status: 'error', msg: 'Data inexistente'}
    }
}
