const express = require('express');
const router = express.Router();
const cavsRepository = require('../repositories/cavs');

router.get('/', function(req, res, next) {
  res.send(cavsRepository.list());
});

router.get('/:cavid', function(req, res, next) {
  const cavId = req.params.cavid;
  const date = req.query.date;
  const category = req.query.category;
  res.send(cavsRepository.listAgenda(cavId, date, category));
});

router.post('/:cavid/:category', function(req, res, next) {
  const cavId = req.params.cavid;
  const date = req.body.date;
  const carId = req.body.carId;
  const hour = req.body.hour;
  const category = req.params.category;

  if (category !== 'visit' && category !== 'inspection') {
    return res.status(404).send('Recurso não encontrado!');
  }

  const result = cavsRepository.schedule(category, cavId, carId, date, hour);
  const status = result && result['status'] === 'success' ? 200 : 400;
  res.status(status).send(result);
});

module.exports = router;
