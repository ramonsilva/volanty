const fs = require('fs');

exports.find = (collection, query) => {
    const list = require('../database/' + collection + '.json');
    if (query) {
        return list.filter(elem => {
            for (const key in query) {
               if(query[key] !== elem[key])  {
                   return false;
               }
            }
            return true;
        })
    }

    return list;
};

exports.update = (collection, update) => {
    const list = require('../database/' + collection + '.json');
    const save = {...list, ...update};

    fs.writeFile(__dirname + '/../database/' + collection + '.json', JSON.stringify(save), err => {
        if (err) throw err;
    });
    return save;
};
