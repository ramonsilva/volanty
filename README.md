Desafio Volanty

Projeto desenvolvido com node, para rodá-lo é necessário que você tenha já instalada na sua máquina, pelo menos, node 8:

- `npm install`
- `npm start`

As rotas de POST que não foram definidos no desafio os parâmetros, seguem esse modelo:
```
{
	"date": "2019-07-17",
	"carId": 5,
	"hour": 15
}
```